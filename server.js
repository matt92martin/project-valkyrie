// example usage: process.env.DB_PASS
require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
mongoose.connect(`mongodb://${process.env.DB_HOST}:27017/test`, { config: { autoIndex: false } });

const path = require('path');

// const renderToString = require('react-dom/server').renderToString;
// const match = require('react-router').match;
// const RouterContext = require('react-router').RouterContext;

const compression = require('compression');
const app = express();

const isProduction = process.env.NODE_ENV === 'production';
const port = isProduction ? 3001 : 3000;
const publicPath = path.resolve(__dirname, 'public');


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {

});


app.use(compression());
app.use(express.static(publicPath));


// We only want to run the workflow when not in production
if (!isProduction) {

    const httpProxy = require('http-proxy');
    const proxy = httpProxy.createProxyServer();
    const fs = require('fs');
    const colors = require('colors');

    if ( fs.existsSync('./public/build/bundle.js') ){
        console.log( 'Make sure to delete your bundle.js to enable hot reloading.'.underline.yellow );
    }

    // We require the bundler inside the if block because it is only needed in a development environment.
    const bundle = require('./server/bundle.js');
    bundle();

    // Any requests to localhost:3000/build is proxied to webpack-dev-server
    app.all('/build/*', function (req, res) {
        proxy.web(req, res, {
            target: 'http://localhost:8080'
        });
    });

    // It is important to catch any errors from the proxy or the server will crash.
    proxy.on('error', function(e) {
        console.log('Could not connect to proxy, please try again...');
    });

}


// app.get('/api/v1', function(req, res){});
// const routes = require('./app/router');

app.get('*', (req, res) => {
    res.send(renderPage('Loading...'));
    // Match url to routes
    // match(
    //     {
    //         routes: routes,
    //         location: req.url
    //     },
    //     (error, redirectLocation, renderProps) => {
    //         if (error) {
    //             res.status(500).send(error.message)
    //         } else if (redirectLocation) {
    //             res.redirect(302, redirectLocation.pathname + redirectLocation.search)
    //         } else if (renderProps) {
    //             // You can also check renderProps.components or renderProps.routes for
    //             // your "not found" component or route respectively, and send a 404 as
    //             // below, if you're using a catch-all route.
    //             res.status(200).send(renderToString(<RouterContext {...renderProps} />))
    //         } else {
    //             res.status(404).send('Not found')
    //         }
    // });
});

function renderPage(appHtml) {
    return `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="Rjeaton,matt92martin">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <title>Table Topped</title>
    
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=1"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material-icons.min.css">
    </head>
    <body>
    
    <div id="js-main">${appHtml}</div>
    
    <script src="/build/bundle.js"></script>
    </body>
    </html>
   `
}

app.listen(port, function () {
    console.log('Server running on port ' + port);
});
