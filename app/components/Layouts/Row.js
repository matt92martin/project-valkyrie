import React, { Component } from 'react';
import classnames from 'classnames';


class Row extends Component {


    render() {


        return (
            <div className={classnames('row', this.props.className)}>
                {this.props.children}
            </div>
        );
    }
}


export default Row;
