import './app.scss';
import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

class App extends Component {

  render() {

    return(
        <div className="app-component">
            <Header/>
            {this.props.children}
            {/*<Footer/>*/}
        </div>
    );

  }

}

export default App;
