import React, { Component } from 'react';
import { Link } from 'react-router';


class PageNotFound extends Component {


    render() {

        return (
            <div>
                <h2>Page Not Found!</h2>
                <p>It looks like you're lost...</p>
                <Link to="/">Go Home?</Link>
            </div>
        );
    }
}


export default PageNotFound;
