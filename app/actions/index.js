import * as types from '../constants/ActionTypes';

export function addToIndex(text) {
    return {
        type: types.ADD_ITEM,
        text
    }
}
