import './index.scss';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Login from '../../components/Login/Login';



class Index extends Component {

    renderTest(){
        return this.props.test.map((item) => {
            return (
                <li className="listItem" key={item.id}>{item.text}</li>
            );
        });
    }

    render() {

        return (
            <div className="index-component">
                <div className="index-content">
                    <h2>Table Tops</h2>
                    <Login />
                    <ul className="index-list">
                        {this.renderTest()}
                    </ul>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state){

    return {
        test: state.test
    };
}

export default connect(mapStateToProps)(Index);
