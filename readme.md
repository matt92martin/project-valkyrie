##Project Valkyrie

The purpose of this project is to sharpen our full-stack development skills by completing the full lifecycle of a web application. 


###Todos

- [x] Configure webpack for development
- [x] Configure webpack for production
- [x] Add test reducer for structure
- [x] Finish basic server-side rendering
- [ ] Create models for all expected data
- [ ] Flush out UI plans
- [ ] Add Working reducer for action
- [ ] Remove nvm installed node
- [ ] Switch node manager to n
- [ ] Add dedupe plugin for webpack
